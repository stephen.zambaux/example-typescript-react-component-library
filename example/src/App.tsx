import "antd/dist/antd.css";
import { Menu } from "antd";
// @ts-ignore
import { useRoutes } from "hookrouter";
import React, { useContext } from "react";

import "./App.css";
import { WithLeftHeaderFooterLayout } from "@fstn/react-library";
import { RoutesContext } from "@fstn/react-library";
import { useImmer } from "use-immer";

function App() {
    const [path, updatePath] = useImmer({});
    setInterval(() => {
        if (path != window.location.href) {
            updatePath(() => window.location.href);
        }
    }, 500);
    const { routesCxt } = useContext(RoutesContext);
    const appCtx = {
        "/app": () => (
            <WithLeftHeaderFooterLayout
                left={
                    <div>
                        <Menu>
                            <Menu.Item title={"Menu 1"}>Menu 1</Menu.Item>
                            <Menu.Item title={"Menu 2"}>Menu 2</Menu.Item>
                            <Menu.Item title={"Menu 3"}>Menu 3</Menu.Item>
                            <Menu.Item title={"Menu 4"}>Menu 4</Menu.Item>
                            <Menu.Item title={"Menu 5"}>Menu 5</Menu.Item>
                        </Menu>
                    </div>
                }
                footer={<div>Footer</div>}
            >
                <> Test de center</>
            </WithLeftHeaderFooterLayout>
        ),
    };
    let routes = useRoutes({ ...appCtx, ...(routesCxt?.routes || {}) });
    return <>{routes}</>;
}

export default App;
