import { Spin } from "antd";
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { AxiosContextProvider } from "@fstn/react-library";
import "./index.css";
import { RoutesContextProvider } from "@fstn/react-library";
import { UserContextProvider } from "@fstn/react-library";
import { Suspense } from "react";

ReactDOM.render(
    <Suspense fallback={<></>}>
        <React.StrictMode>
            <AxiosContextProvider>
                <UserContextProvider>
                    <RoutesContextProvider>
                        <App />
                    </RoutesContextProvider>
                </UserContextProvider>
            </AxiosContextProvider>
        </React.StrictMode>
    </Suspense>,
    document.getElementById("root")
);
