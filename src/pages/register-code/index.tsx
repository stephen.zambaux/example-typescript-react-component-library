import React from "react";
import { CenterLayout } from "../../components/center-layout";
import { ImageOnSide } from "../../components/image-on-side";
import { RegisterCodeForm } from "../../forms/register-code-form";
import {SecurityImage} from "../../images/security-image";

export function RegisterCodePage(props: { email: string; code?: string; image?: JSX.Element }) {
    return (
        <CenterLayout>
            <ImageOnSide image={props.image || <SecurityImage />}>
                <RegisterCodeForm email={props.email} code={props.code} />
            </ImageOnSide>
        </CenterLayout>
    );
}
