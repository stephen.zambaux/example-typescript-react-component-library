import React from "react";
import { Delayed } from "../../components/Delayed";
import { ImageOnSide } from "../../components/image-on-side";
import { LoadingImage } from "../../images/loading-image";

export function LoadingPage(props: { image?: JSX.Element; email?: string; code?: string }) {
    return (
        <Delayed waitBeforeShow={1000}>
            <ImageOnSide image={props.image || <LoadingImage />}>
                <></>
            </ImageOnSide>
        </Delayed>
    );
}
