import React from "react";
import { CenterLayout } from "../../components/center-layout";
import { ImageOnSide } from "../../components/image-on-side";
import { RegisterEmailForm } from "../../forms/register-email-form";
import { Signup } from "../../images/Signup";

export function RegisterEmailPage(props: { email?: string; code?: string; image?: JSX.Element }) {
    return (
        <CenterLayout>
            <ImageOnSide image={props.image || <Signup />}>
                <RegisterEmailForm email={props.email} code={props.code} />
            </ImageOnSide>
        </CenterLayout>
    );
}
