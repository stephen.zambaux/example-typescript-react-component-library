import React from "react";
import { CenterLayout } from "../../components/center-layout";
import { ImageOnSide } from "../../components/image-on-side";
import { LoginForm } from "../../forms/login-form";
import { Signin } from "../../images/Signin";

export function LoginPage(props: { image?: JSX.Element; email?: string }) {
    return (
        <CenterLayout>
            <ImageOnSide image={props.image || <Signin />}>
                <LoginForm email={props.email} />
            </ImageOnSide>
        </CenterLayout>
    );
}
