import React from "react";
import { CenterLayout } from "../../components/center-layout";
import { ImageOnSide } from "../../components/image-on-side";
import { ForgotPasswordCodeForm } from "../../forms/forgot-password-code-form";
import { SecurityImage } from "../../images/security-image";

export function ForgotPasswordCodePage(props: { image?: JSX.Element; email: string }) {
    return (
        <CenterLayout>
            <ImageOnSide image={props.image || <SecurityImage />}>
                <ForgotPasswordCodeForm email={props.email} />
            </ImageOnSide>
        </CenterLayout>
    );
}
