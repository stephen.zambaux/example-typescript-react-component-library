import React from "react";
import { CenterLayout } from "../../components/center-layout";
import { ImageOnSide } from "../../components/image-on-side";
import { SignInUpForm } from "../../forms/sign-in-up-form";
import { Signin } from "../../images/Signin";

export function SignInUpPage(props: { image?: JSX.Element }) {
    return (
        <CenterLayout>
            <ImageOnSide image={props.image || <Signin />}>
                <SignInUpForm />
            </ImageOnSide>
        </CenterLayout>
    );
}
