import React from "react";
import { CenterLayout } from "../../components/center-layout";
import { ImageOnSide } from "../../components/image-on-side";
import { RegisterForm } from "../../forms/register-form";
import { FinishSignup } from "../../images/FinishSignup";

export function RegisterPage(props: { image?: JSX.Element; email?: string; code?: string }) {
    return (
        <CenterLayout>
            <ImageOnSide image={props.image || <FinishSignup />}>
                <RegisterForm
                    email={props.email}
                    code={props.code}
                    hiddenFields={["phone", "gender"]}
                />
            </ImageOnSide>
        </CenterLayout>
    );
}
