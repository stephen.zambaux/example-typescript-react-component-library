import React from "react";
import { CenterLayout } from "../../components/center-layout";
import { ImageOnSide } from "../../components/image-on-side";
import { ForgotPasswordEmailForm } from "../../forms/forgot-password-email-form";
import { ForgotPassword } from "../../images/ForgotPassword";

export function ForgotPasswordPage(props: { image?: JSX.Element }) {
    return (
        <CenterLayout>
            <ImageOnSide image={props.image || <ForgotPassword />}>
                <ForgotPasswordEmailForm />
            </ImageOnSide>
        </CenterLayout>
    );
}
