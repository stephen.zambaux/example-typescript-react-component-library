import React, { ReactNode } from "react";
import { useImmer } from "use-immer";
import { ColorContextType  } from "../interfaces";

export const ColorContext = React.createContext({} as any);
export const ColorContextProvider = (props: { children: any, primary : string, logo: ReactNode }) => {
    const [colorCtx, updateColorCtx] = useImmer<any>({primary:props.primary,logo:props.logo} as ColorContextType);

    return (
        <ColorContext.Provider value={{ colorCtx, updateColorCtx }}>
            {props?.children}
        </ColorContext.Provider>
    );
};
