// @ts-ignore
import { usePath } from "hookrouter";
import React, { useEffect } from "react";
import { useImmer } from "use-immer";
import { useUser } from "../hooks/user";
import { UserContextType, UserContextTypeContent } from "../interfaces";
import { LoadingPage } from "../pages/loading-page";
import { check } from "../utils/path";

export const UserContext = React.createContext({} as UserContextType);
export const UserContextProvider = (props: { children: any; publicPagesPattens?: string[] }) => {
    const [userCtx, updateUserCtx] = useImmer<UserContextTypeContent>({ initialized: false });
    const { loadMe } = useUser({ userCtx, updateUserCtx });
    const path = usePath();

    useEffect(() => {
        if (props.publicPagesPattens && check(path, props.publicPagesPattens) > 0) {
            updateUserCtx((draft) => {
                draft.initialized = true;
            });
        } else {
            loadMe().then();
        }
    }, []);

    if (!userCtx.initialized) {
        return <LoadingPage />;
    }

    return (
        <UserContext.Provider value={{ userCtx, updateUserCtx }}>
            {props?.children}
        </UserContext.Provider>
    );
};
