import React from 'react';
import styled from 'styled-components'

const Style = styled.div`
    min-width: 14px;
    margin-right: 10px;
    font-size: 14px;
    display: inline-block;`

Tracked.propTypes = {};

export function Tracked() {
    return (
        <Style>
            <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0)">
                    <path
                        d="M5.46875 11.0078V13.9066C5.46875 14.1091 5.59875 14.2884 5.79125 14.3522C5.83938 14.3678 5.88875 14.3753 5.9375 14.3753C6.08375 14.3753 6.225 14.3066 6.315 14.1841L8.01063 11.8766L5.46875 11.0078Z"
                        fill="#828282"/>
                    <path
                        d="M14.8027 0.0859538C14.659 -0.0159212 14.4702 -0.0296712 14.314 0.0522038L0.251475 7.39595C0.0852255 7.48283 -0.0128994 7.66033 0.000850552 7.8472C0.0152255 8.0347 0.139601 8.1947 0.316475 8.25533L4.22585 9.59158L12.5515 2.47283L6.10897 10.2347L12.6608 12.4741C12.7096 12.4903 12.7608 12.4991 12.8121 12.4991C12.8971 12.4991 12.9815 12.476 13.0558 12.431C13.1746 12.3585 13.2552 12.2366 13.2758 12.0997L14.9946 0.537204C15.0202 0.362204 14.9465 0.188454 14.8027 0.0859538Z"
                        fill="#828282"/>
                </g>
                <defs>
                    <clipPath id="clip0">
                        <rect width="15" height="15" fill="white"/>
                    </clipPath>
                </defs>
            </svg>
        </Style>
    );
}
