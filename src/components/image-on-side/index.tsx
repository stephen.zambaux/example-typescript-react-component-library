import React, { useContext } from "react";
import styled from "styled-components";
// @ts-ignore
import useMobileDetect from "use-mobile-detect-hook";
import { ColorContext } from "../../color-context";
import { ColorContextType } from "../../interfaces";

const Style: any = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    flex-direction: ${(props: any) => (props.imageOnLeft ? "row-reverse" : "row")};
    
    h1{
        font-weight: 600;
        font-size: 25px;
        line-height: 34px;
        color: #000000;
        text-align: center;
    }
    
    .image-container{
        max-width: 50%;
        flex: 1 1 0%;
        min-height: 100%;
        display: flex;
        align-items: center;
        text-align: center;
        background: #F3F5F7;        
        align-self: stretch;
    }
    .image-container svg {
        width: 70%;
        margin: auto;
    }
    
    @media screen and (max-width: 768px) {    
        .image-container{
          display: none;
        }
    }
    }
    .logo{
      margin: 2em;
    }
    
    .ant-card{
    
    }
    
    .cgu{
        font-style: normal;
        font-weight: normal;
        font-size: 12px;
        line-height: 16px;
        text-align: center;        
        color: #7B7B7B;
        }

`;

export function ImageOnSide(props?: { image: JSX.Element; children: any; imageOnLeft?: boolean }) {
    const detectMobile = useMobileDetect();
    const { colorCtx } = useContext<ColorContextType>(ColorContext);

    return (
        <Style imageOnLeft={props?.imageOnLeft}>
            <div
                style={{
                    maxWidth: detectMobile.isMobile() ? "100%" : "50%",
                    flex: "2 0 200px",
                    padding: "1em",
                    alignItems: "center",
                    textAlign: "center",
                    maxHeight: "100%",
                    overflow: "auto",
                }}
            >
                <div className={"logo"}>{colorCtx?.logo}</div>
                {props?.children}
            </div>
            {!detectMobile.isMobile() && (
                <div className={"image-container"} style={{ maxWidth: "50%", flex: "1 1" }}>
                    {props?.image}
                </div>
            )}
        </Style>
    );
}
