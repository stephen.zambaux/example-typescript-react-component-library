import styled from "styled-components";

export const layout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
};
export const tailLayout = {
    wrapperCol: { offset: 0, span: 24 },
};

export const FormStyle = styled.div`
    @media screen and (max-width: 768px) {
        .ant-card-body {
            padding: 4px;
        }

        .ant-input-wrapper.ant-input-group {
            display: flex;
            flex-flow: column;
            gap: 1em;
        }
    }

    .ant-btn {
        min-width: 120px;
    }
    .ant-form-item-label label {
        margin-right: 1em;
        overflow: hidden;
        max-width: 100%;
        width: 100%;
        min-width: 100%;
    }

    .right .ant-form-item-control-input-content {
        text-align: right;
    }

    .ant-form-item {
    }

    .ant-input-affix-wrapper,
    input,
    button {
        border: none;
    }

    h1 {
        text-align: left;
        width: 100%;
        margin-bottom: 9px;
        font-weight: 600;
        font-size: 25px;
    }

    .ant-input-affix-wrapper,
    input {
        background: #f3f5f7 !important;
        border-radius: 3px;
    }

    .ant-input-affix-wrapper input {
        min-height: 41px !important;
    }

    .ant-input-affix-wrapper,
    button,
    input {
        min-height: 51px;
        align-items: center;
    }

    .ant-btn,
    button {
        background-color: #0e5396;
    }
`;
