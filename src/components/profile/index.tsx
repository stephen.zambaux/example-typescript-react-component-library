import { UserOutlined } from "@ant-design/icons";
import { Avatar, Popconfirm } from "antd";
import React, { useContext } from "react";
import styled from "styled-components";
import { useUser } from "../../hooks/user";
import { UserContextType } from "../../interfaces";
import { UserContext } from "../../user-context";

const Style = styled.div`  
    display: flex;
    align-items: center;
    width: 100%;
    justify-content: space-evenly;
    padding: 1em;
    color: #828282;
    font-size: 14px;
    :hover{
    background-color: #EEE;
    }
  .name{
      text-overflow: ellipsis;
      overflow: hidden;
      max-width: 120px;
  } 
  .content{  
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;

Profile.propTypes = {};

export function Profile() {
    const { userCtx } = useContext<UserContextType>(UserContext);
    const { logout } = useUser(useContext<UserContextType>(UserContext));

    return (
        <Style>
            {userCtx?.user &&
            <Popconfirm
                title={<>Do you want logout?</>}
                onConfirm={logout}>
                <div
                    className={"content"}
                    style={{ cursor: "pointer" }}>
                    <Avatar size={"small"} icon={<UserOutlined {...{} as any} />} />
                    <span className={"name"}>{userCtx?.user.firstName + " " + userCtx?.user.lastName}</span>
                </div>
            </Popconfirm>
            }
        </Style>
    );
}

