import React, { ReactNode } from "react";
import styled from "styled-components";

const Style = styled.div`
    background: #f8f8f8;
    margin: auto;
    height: 100%;
    text-align: center;
    display: grid;
    grid-template-areas:
        "header"
        "left"
        "center"
        "footer";
    grid-template-rows: minmax(0, min-content) minmax(0, min-content) minmax(0, 100%) minmax(
            0,
            min-content
        );
    grid-template-columns: 1fr;


    @media screen and (min-width: 768px) {
        display: grid;
        grid-template-areas:
            "header header"
            "left center"
            "left footer";
        grid-template-rows: minmax(min-content, max-content) minmax(0, 100%) minmax(0, max-content);
        grid-template-columns: minmax(min-content, max-content) 1fr;
    }
    
    > div {
        background: white;
        padding: 2em;
        margin: 1em;
    }
    
    .center{
      overflow: auto;
      max-width: 100%;
      max-height: 100%;
    }
`;

export function WithLeftHeaderFooterLayout(props: {
    children: ReactNode;
    header?: ReactNode;
    left?: ReactNode;
    footer?: ReactNode;
}) {
    return (
        <Style>
            {props.header && <div style={{ gridArea: "header" }}>{props.header}</div>}
            {props.left && (
                <div style={{ gridArea: "left", marginLeft: "0", paddingLeft: 0, paddingRight: 0 }}>
                    {props.left}
                </div>
            )}
            <div className={"center"} style={{ gridArea: "center" }}>{props.children}</div>
            {props.footer && <div style={{ gridArea: "footer" }}>{props.footer}</div>}
        </Style>
    );
}
