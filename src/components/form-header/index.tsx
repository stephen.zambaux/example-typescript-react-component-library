import React, { ReactNode } from "react";
import styled from "styled-components";

const Style = styled.div`
    font-weight: 400;
    margin-bottom: 1em;
    font-size: 14px;
    line-height: 19px;
    text-align: center;
    color: #7b7b7b;
`;

export function FormHeader(props: { children: ReactNode }) {
    return <Style>{props.children}</Style>;
}
