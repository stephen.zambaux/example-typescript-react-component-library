import { omit } from "lodash";
import React from "react";
import styled from "styled-components";

const Style = styled.div`
    background: #f8f8f8;
    margin: auto;
    height: 100%;
    text-align: center;
    display: flex;
    align-items: center;

    .container {
        display: flex;
        align-items: center;
        background: white;
        width: 100%;
        height: 100%;
        @media screen and (min-width: 768px) {
        }
    }
`;

export function CenterLayout(props: any) {
    return (
        <Style {...omit(props, "children")}>
            <div className={"container"}>{props.children}</div>
        </Style>
    );
}
