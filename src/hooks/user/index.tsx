import { notification } from "antd";
// @ts-ignore
import React, { useContext } from "react";
import { AxiosContext, AxiosContextType } from "../../axios-context";
import { UserContextType } from "../../interfaces";
import { LocaleHelp, LocaleTitle } from "../../locale";

export function useUser(userContext: UserContextType): any {
    const { updateToken } = useContext<AxiosContextType>(AxiosContext);
    const { axios } = useContext<AxiosContextType>(AxiosContext);

    const loadMe = async () => {
        try {
            let config = {};
            const res = await axios?.get(`/me`, config);
            if (res?.data) {
                localStorage.setItem("accessToken", res.data.token);
                await userContext.updateUserCtx((draft: any) => {
                    draft.user = res.data;
                    draft.token = res.data.token;
                    draft.initialized = true;
                });
            } else {
                await userContext.updateUserCtx((draft: any) => {
                    draft.initialized = true;
                });
            }
            return Promise.resolve();
        } catch (error) {
            userContext.updateUserCtx((draft: any) => {
                draft.token = undefined;
                draft.initialized = true;
            });
            if (window.location.pathname !== "/login") {
                //navigate("/login-form-page")s
                return Promise.reject(error);
            } else {
                return Promise.resolve();
            }
        }
    };
    const login = async (email: string, password: string, remember: string) => {
        try {
            localStorage.removeItem("accessToken");
            const res = await axios?.post(
                `/login`,
                {
                    username: email || "candidate@yopmail.com",
                    password: password || "test",
                    remember,
                },
                {}
            );
            if (res?.status! < 300) {
                const accessToken = res?.data.access_token;
                await updateToken?.(accessToken);
                return res?.data.payload;
            }
        } catch (e) {
            if (e?.response?.status === 401) {
                await updateToken?.(undefined);
                notification.error({
                    message: <LocaleTitle tkey={"login-page.invalid"} />,
                    description: <LocaleHelp tkey={"login-page.invalid"} />,
                });
            } else {
                notification.warn({
                    message: <LocaleTitle tkey={"server.error"} />,
                    description: <LocaleHelp tkey={"server.error"} />,
                });
            }
            throw e;
        }
    };

    const registerEmail = async (email: string) => {
        try {
            return await axios?.post(
                `/register-email`,
                {
                    email: email || "candidate@yopmail.com",
                },
                {}
            );
        } catch (e) {
            if (e?.response?.status === 401) {
                await updateToken?.(undefined);
                notification.error({
                    message: <LocaleTitle tkey={"register-email-page.invalid"} />,
                    description: <LocaleHelp tkey={"register-email-page.invalid"} />,
                });
            } else if (e?.response?.status === 409) {
                notification.warn({
                    message: <LocaleTitle tkey={"already-exists.error"} />,
                    description: <LocaleHelp tkey={"already-exists.error"} />,
                });
            } else {
                notification.warn({
                    message: <LocaleTitle tkey={"server.error"} />,
                    description: <LocaleHelp tkey={"server.error"} />,
                });
            }
            throw e;
        }
    };

    const forgetPasswordEmail = async (email: string) => {
        try {
            return await axios?.post(
                `/forgot-password-email`,
                {
                    email: email || "candidate@yopmail.com",
                },
                {}
            );
        } catch (e) {
            if (e?.response?.status === 401) {
                await updateToken?.(undefined);
                notification.error({
                    message: <LocaleTitle tkey={"forgot-password-email-page.invalid"} />,
                    description: <LocaleHelp tkey={"forgot-password-email-page.invalid"} />,
                });
            } else {
                notification.warn({
                    message: <LocaleTitle tkey={"server.error"} />,
                    description: <LocaleHelp tkey={"server.error"} />,
                });
            }
            throw e;
        }
    };

    const preLogin = async (email: string) => {
        try {
            return (
                await axios?.post(
                    `/pre-login`,
                    {
                        email: email,
                    },
                    {}
                )
            )?.data;
        } catch (e) {
            notification.warn({
                message: <LocaleTitle tkey={"server.error"} />,
                description: <LocaleHelp tkey={"server.error"} />,
            });
            throw e;
        }
    };

    const forgetPasswordCode = async (email: string, code: string, password: string) => {
        try {
            return await axios?.post(
                `/forgot-password`,
                {
                    email: email,
                    code: code,
                    password: password,
                },
                {}
            );
        } catch (e) {
            if (e?.response?.status === 401) {
                await updateToken?.(undefined);
                notification.error({
                    message: <LocaleTitle tkey={"forgot-password-code-page.invalid"} />,
                    description: <LocaleHelp tkey={"forgot-password-code-page.invalid"} />,
                });
            } else {
                notification.warn({
                    message: <LocaleTitle tkey={"server.error"} />,
                    description: <LocaleHelp tkey={"server.error"} />,
                });
            }
            throw e;
        }
    };

    const registerCode = async (email: string, code: string) => {
        try {
            return await axios?.post(
                `/register-code`,
                {
                    email: email,
                    code: code,
                },
                {}
            );
        } catch (e) {
            if (e?.response?.status === 401) {
                await updateToken?.(undefined);
                notification.error({
                    message: <LocaleTitle tkey={"register-code-page.invalid"} />,
                    description: <LocaleHelp tkey={"register-code-page.invalid"} />,
                });
            } else {
                notification.warn({
                    message: <LocaleTitle tkey={"server.error"} />,
                    description: <LocaleHelp tkey={"server.error"} />,
                });
            }
            throw e;
        }
    };

    const register = async (registerData: any) => {
        try {
            return await axios?.post(
                `/register`,
                {
                    ...registerData,
                },
                {}
            );
        } catch (e) {
            if (e?.response?.status === 401) {
                await updateToken?.(undefined);
                notification.error({
                    message: <LocaleTitle tkey={"register-page.invalid"} />,
                    description: <LocaleHelp tkey={"register-page.invalid"} />,
                });
            } else {
                notification.warn({
                    message: <LocaleTitle tkey={"server.error"} />,
                    description: <LocaleHelp tkey={"server.error"} />,
                });
            }
            throw e;
        }
    };

    const logout = async () => {
        try {
            await axios?.delete(`/login`, {});
        } catch (e) {}
        localStorage.removeItem("accessToken");
        userContext.updateUserCtx((draft: any) => {
            draft.user = undefined;
            draft.token = undefined;
            draft.account = undefined;
        });
        window.location.href = "/login";
    };

    return {
        register,
        registerCode,
        registerEmail,
        forgetPasswordEmail,
        forgetPasswordCode,
        loadMe,
        login,
        logout,
        preLogin,
    };
}
