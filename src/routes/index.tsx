import React, { useContext } from "react";
import { useImmer } from "use-immer";
import { UserContextType } from "../interfaces";
import { ForgotPasswordCodePage } from "../pages/forgot-password-code-page";
import {ForgotPasswordPage} from "../pages/forgot-password-page";
import {LoginPage} from "../pages/login-page";
import {RegisterCodePage} from "../pages/register-code";
import {RegisterEmailPage} from "../pages/register-email-page";
import {RegisterPage} from "../pages/register-page";
import { SignInUpPage } from "../pages/sign-in-up-page";
import { UserContext, UserContextProvider } from "../user-context";
// @ts-ignore
import { navigate } from "hookrouter";


type RoutesContextTypeContent = { routes?: any; token?: string };
export type RoutesContextType = { routesCxt?: RoutesContextTypeContent; updateRoutesCtx?: any };
export const RoutesContext = React.createContext({} as RoutesContextType);
export const RoutesContextProvider = (props: { children: any }) => {

    const {userCtx} = useContext<UserContextType>(UserContext)
    const loginRoutesDef = {
        "/": () => {
            if(!!userCtx?.user){
                navigate('welcome')
            }
            return(
                <UserContextProvider>
                    <SignInUpPage />
                </UserContextProvider>
            )
        },
        "/login/:email": (props:{email:string}) => (
            <UserContextProvider>
                <LoginPage email={props.email} />
            </UserContextProvider>
        ),
        "/login": () => (
            <UserContextProvider>
                <LoginPage/>
            </UserContextProvider>
        ),
        "/register-email": () => <RegisterEmailPage />,
        "/register-code/:email": ({ email }: any) => <RegisterCodePage email={email} />,
        "/register/:email/:code": ({ email, code }: any) => <RegisterPage email={email} code={code}  />,
        "/forgot-password-email": () => <ForgotPasswordPage />,
        "/forgot-password-code/:email": (props:{email:string}) => <ForgotPasswordCodePage email={props.email}/>,
    };


    const [routesCxt, updateRoutesCtx] = useImmer<RoutesContextTypeContent>({
        routes: loginRoutesDef,
    });
    console.log("routes",routesCxt, RoutesContext)

    return (
        <RoutesContext.Provider value={{ routesCxt, updateRoutesCtx }}>
            {props?.children}
        </RoutesContext.Provider>
    );
};
