import { check } from "./path";

describe("Utils", () => {
    beforeEach(async () => {});

    it("check should match", () => {
        const path =
            "/deals/b692551b-91e7-4a64-94be-6af974e9f4a0/a30a0755-5a6d-4a15-9418-55bfe07114c6";
        const pattern = ["/deals/"];

        expect(check(path, pattern)).toBe(1);
    });
    it("check should not match", () => {
        const path =
            "/deals/b692551b-91e7-4a64-94be-6af974e9f4a0/a30a0755-5a6d-4a15-9418-55bfe07114c6";
        const pattern = ["/pop/"];
        expect(check(path, pattern)).toBe(0);
    });
});
