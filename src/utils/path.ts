/**
 * Returns an integer representing the number of items in the patterns
 * array that contain the target string text
 */
export function check(str: string, patterns: string[]) {
    return patterns.reduce(function (previous, current) {
        return previous + (str.startsWith(current) ? 1 : 0);
    }, 0);
}
