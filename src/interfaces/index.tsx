import { ReactNode } from "react";

export type UserContextTypeContent = { user?: any; token?: string, initialized?: boolean };
export type UserContextType = { userCtx?: UserContextTypeContent; updateUserCtx?: any };
export type ColorContextType = { colorCtx?: ColorContextTypeContent; updateColorCtx?: any };
export type ColorContextTypeContent = { primary: string, logo?: ReactNode};
