import { SecurityScanOutlined } from "@ant-design/icons";
import { Button, Card, Form, Input, notification } from "antd";
import { useForm } from "antd/lib/form/Form";
// @ts-ignore
import { navigate } from "hookrouter";
// @ts-ignore
import React, { useContext } from "react";
import styled from "styled-components";
import { useImmer } from "use-immer";
import { FormHeader } from "../../components/form-header";
import { FormStyle, layout } from "../../components/form-layout";
import { useUser } from "../../hooks/user";
import { UserContextType } from "../../interfaces";
import { LocaleButton, LocaleHelp, LocaleLabel, LocaleTitle } from "../../locale";
import { UserContext } from "../../user-context";

const Style = styled(Card).attrs({ bordered: false })``;

export function RegisterCodeForm(
    props: {
        code?: string;
        email?: string;
    } = {}
) {
    const [state, updateState] = useImmer({ captcha: false, loading: false });

    const [form] = useForm();
    const { registerCode } = useUser(useContext<UserContextType>(UserContext));
    const onFinish = async (values: any) => {
        try {
            updateState((draft: any) => {
                draft.loading = true;
            });
            const result = await registerCode(values.email, values.code);
            if (!result) {
                notification.warn({ message: <LocaleHelp tkey={"validate.code.invalid"} /> });
                return;
            }
            navigate(`/register/${values.email}/${values.code}`);
        } catch (e) {
            throw e;
        } finally {
            updateState((draft: any) => {
                draft.loading = false;
            });
        }
    };

    return (
        <FormStyle>
            <Style>
                <h1>
                    <LocaleTitle tkey={"registerCode"} />
                </h1>
                <Form form={form} onFinish={onFinish} {...layout}>
                    <Form.Item
                        hidden={true}
                        name={"email"}
                        initialValue={props.email}
                        label={<LocaleLabel tkey={"email"} />}
                        required={true}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item>
                        <FormHeader>
                            <LocaleHelp tkey={"registerCode"} />
                        </FormHeader>
                    </Form.Item>
                    <Form.Item
                        name={"code"}
                        initialValue={props.code}
                        required={true}
                        rules={[{ required: true, len: 4 }]}
                    >
                        <Input
                            placeholder="1234"
                            size="large"
                            type={"number"}
                            suffix={<SecurityScanOutlined {...({} as any)} />}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button
                            style={{ width: "100%" }}
                            size="large"
                            type={"primary"}
                            loading={state.loading}
                            onClick={() => form.submit()}
                        >
                            <LocaleButton tkey={"registerCode"} />
                        </Button>
                    </Form.Item>
                    <Form.Item>
                        <div className={"cgu"}>
                            <LocaleHelp tkey={"signup.footer"} />
                        </div>
                    </Form.Item>
                </Form>
            </Style>
        </FormStyle>
    );
}
