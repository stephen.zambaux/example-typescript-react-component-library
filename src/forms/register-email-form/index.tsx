import { MailOutlined as MailOutlinedI } from "@ant-design/icons";
import { Button, Card, Form, Input } from "antd";
// @ts-ignore
import { navigate } from "hookrouter";
// @ts-ignore
import React, { useContext } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import styled from "styled-components";
import { useImmer } from "use-immer";
import { FormHeader } from "../../components/form-header";
import { FormStyle, layout, tailLayout } from "../../components/form-layout";
import { useUser } from "../../hooks/user";
import { UserContextType } from "../../interfaces";
import { LocaleHelp, LocaleLabel, LocaleTitle } from "../../locale";
import { UserContext } from "../../user-context";

const Style = styled(Card).attrs({ bordered: false })``;

const MailOutlined = MailOutlinedI as any;
export function RegisterEmailForm(
    props: {
        code?: string;
        email?: string;
    } = {}
) {
    const { registerEmail } = useUser(useContext<UserContextType>(UserContext));
    const [state, updateState] = useImmer({ captcha: false, loading: false });
    const onFinish = async (values: any) => {
        try {
            updateState((draft: any) => {
                draft.loading = true;
            });
            await registerEmail(values.email);
            navigate(`/register-code/${values.email}`);
        } catch (e) {
            throw e;
        } finally {
            updateState((draft: any) => {
                draft.loading = false;
            });
        }
    };

    return (
        <FormStyle>
            <Style>
                <h1>
                    <LocaleTitle tkey={"registerEmail"} />
                </h1>
                <Form onFinish={onFinish} {...layout} scrollToFirstError>
                    <Form.Item>
                        <FormHeader>
                            <LocaleHelp tkey={"registerEmail"} />
                        </FormHeader>
                    </Form.Item>
                    <Form.Item
                        name={"email"}
                        label={<LocaleLabel tkey={"email"} />}
                        initialValue={props.email}
                        required={true}
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                type: "email",
                                message: "The input is not valid Email!",
                            },
                        ]}
                    >
                        <Input prefix={<MailOutlined />} />
                    </Form.Item>
                    <Form.Item
                        hasFeedback
                        labelCol={{ span: 0 }}
                        wrapperCol={{ span: 24 }}
                        required={true}
                    >
                        <div style={{ width: "fit-content", marginRight: 0, marginLeft: "auto" }}>
                            <ReCAPTCHA
                                size={"normal"}
                                sitekey="6LdxSx0aAAAAAITN1byVT-_x8ihn8spFZT9tbgyF"
                                onChange={() =>
                                    updateState((draft: any) => {
                                        draft.captcha = true;
                                    })
                                }
                            />
                        </div>
                    </Form.Item>
                    <Form.Item {...tailLayout}>
                        <Button
                            style={{ width: "100%" }}
                            size="large"
                            type={"primary"}
                            loading={state.loading}
                            disabled={!state.captcha}
                            htmlType={"submit"}
                        >
                            <LocaleLabel tkey={"registerEmail"} />
                        </Button>
                    </Form.Item>
                </Form>
            </Style>
        </FormStyle>
    );
}
