import { UserOutlined } from "@ant-design/icons";
import { Card, Form } from "antd";
import Button from "antd/lib/button";
import { useForm } from "antd/lib/form/Form";
import Input from "antd/lib/input";
// @ts-ignore
import { navigate } from "hookrouter";
import React, { useContext } from "react";
import styled from "styled-components";
import { useImmer } from "use-immer";
import { FormHeader } from "../../components/form-header";
import { FormStyle } from "../../components/form-layout";
import { useUser } from "../../hooks/user";
import { UserContextType } from "../../interfaces";
import { LocaleButton, LocaleHelp, LocalePlaceHolder, LocaleTitle } from "../../locale";
import { UserContext } from "../../user-context";

const Style = styled(Card).attrs({ bordered: false })``;

export function ForgotPasswordEmailForm() {
    const { forgetPasswordEmail, registerEmail, preLogin } = useUser(
        useContext<UserContextType>(UserContext)
    );
    const [form] = useForm();

    // @ts-ignore
    const [state, updateState] = useImmer({ loading: false });
    const onFinish = async (values: any) => {
        updateState((draft: any) => {
            draft.loading = true;
        });
        try {
            const userExists = await preLogin(values.email);
            if (userExists) {
                await forgetPasswordEmail(values.email);
                navigate(`/forgot-password-code/${values.email}`);
            } else {
                await registerEmail(values.email);
                navigate(`/register-code/${values.email}`);
            }
        } finally {
            updateState((draft: any) => {
                draft.loading = false;
            });
        }
    };

    return (
        <FormStyle>
            <Style>
                <h1>
                    <LocaleTitle tkey={"forgotPasswordEmail"} />
                </h1>
                <Form
                    form={form}
                    name="control-hooks"
                    onFinish={onFinish}
                    style={{ maxWidth: "450px" }}
                >
                    <Form.Item>
                        <FormHeader>
                            <LocaleHelp tkey={"forgotPasswordEmail"} />
                        </FormHeader>
                    </Form.Item>
                    <Form.Item name={"email"} rules={[{ type: "email", required: true }]}>
                        <Input
                            placeholder={LocalePlaceHolder({ tkey: "email" })}
                            size="large"
                            type={"email"}
                            suffix={<UserOutlined {...({} as any)} />}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button
                            style={{ width: "100%" }}
                            size="large"
                            type={"primary"}
                            loading={state.loading}
                            onClick={() => form.submit()}
                        >
                            <LocaleButton tkey={"forgotPasswordEmail"} />
                        </Button>
                    </Form.Item>
                </Form>
            </Style>
        </FormStyle>
    );
}
