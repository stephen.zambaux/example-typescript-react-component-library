import { SafetyCertificateOutlined, SecurityScanOutlined } from "@ant-design/icons";
import { Card, Form, Input, notification } from "antd";
import { useForm } from "antd/lib/form/Form";
import Search from "antd/lib/input/Search";
// @ts-ignore
import { navigate } from "hookrouter";
import React, { useContext } from "react";
import styled from "styled-components";
import { useImmer } from "use-immer";
import { FormHeader } from "../../components/form-header";
import { FormStyle, layout } from "../../components/form-layout";
import { useUser } from "../../hooks/user";
import { UserContextType } from "../../interfaces";
import { LocaleButton, LocaleHelp, LocalePlaceHolder, LocaleTitle } from "../../locale";
import { UserContext } from "../../user-context";

const Style = styled(Card).attrs({ bordered: false })`
    h1 {
        text-align: left;
        width: 100%;
        margin-bottom: 1em;
        font-weight: bold;
    }
`;

export function ForgotPasswordCodeForm(
    props: {
        code?: string;
        email?: string;
    } = {}
) {
    const { forgetPasswordCode } = useUser(useContext<UserContextType>(UserContext));
    const [form] = useForm();

    // @ts-ignore
    const [state, updateState] = useImmer({ loading: false });
    const onFinish = async (values: any) => {
        updateState((draft: any) => {
            draft.loading = true;
        });
        try {
            const result = await forgetPasswordCode(values.email, values.code, values["New Password"]);
            if (!result) {
                notification.warn({ message: <LocaleHelp tkey={"forgotPasswordCode.invalid"} /> });
                return;
            }
            navigate(`/login/${values.email}`);
        } finally {
            updateState((draft: any) => {
                draft.loading = false;
            });
        }
    };

    return (
        <FormStyle>
            <Style>
                <h1>
                    <LocaleTitle tkey={"forgotPasswordCode"} />
                </h1>
                <Form form={form} onFinish={onFinish} {...layout}>
                    <Form.Item hidden={true}
                               name={"email"}
                               initialValue={props.email}
                               required={true}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item>
                        <FormHeader><LocaleHelp tkey={"forgotPasswordCode"} /></FormHeader>
                    </Form.Item>
                    <Form.Item
                        name={"New Password"}
                        required={true}
                        rules={[{ min: 10, required: true, message: "Please enter your new password (min 10 characters)" }]}
                    >
                        <Input
                            placeholder={LocalePlaceHolder({ tkey: "newPassword" })}
                            size={"large"}
                            type={"password"}
                            prefix={<SafetyCertificateOutlined {...({} as any)} />}
                        />
                    </Form.Item>
                    <Form.Item
                        name={"code"}
                        initialValue={props.code}
                        required={true}
                        rules={[{ required: true, len: 4 }]}
                    >
                        <Search
                            placeholder={LocalePlaceHolder({ tkey: "code" })}
                            enterButton={<LocaleButton tkey={"forgotPasswordCode"} />}
                            size="large"
                            loading={state.loading}
                            type={"number"}
                            suffix={<SecurityScanOutlined {...({} as any)} />}
                            onSearch={() => form.submit()}
                        />
                    </Form.Item>
                </Form>
            </Style>
        </FormStyle>
    );
}
