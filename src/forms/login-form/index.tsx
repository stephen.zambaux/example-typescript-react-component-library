import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Card, Input, notification } from "antd";
import Button from "antd/lib/button";
// noinspection ES6UnusedImports
import Checkbox from "antd/lib/checkbox";
import Form from "antd/lib/form";
import { useForm } from "antd/lib/form/Form";
// @ts-ignore
// @ts-ignore
// @ts-ignore
import React, { useContext } from "react";
import styled from "styled-components";
import { useImmer } from "use-immer";
import { FormHeader } from "../../components/form-header";
import { FormStyle } from "../../components/form-layout";
import { useUser } from "../../hooks/user";
import { UserContextType } from "../../interfaces";
import {
    LocaleButton,
    LocaleHelp,
    LocaleLabel,
    LocalePlaceHolder,
    LocaleTitle,
} from "../../locale";
import { Translate } from "../../translation-hook";
import { UserContext } from "../../user-context";

const Style = styled(Card).attrs({ bordered: false })`
    width: fit-content;
    margin: auto;
    margin: auto;
    flex: none;
`;

export function LoginForm(props: { email?: string }) {
    const { login } = useUser(useContext<UserContextType>(UserContext));
    const { t } = Translate();
    const [form] = useForm();
    const [state, updateState] = useImmer({ loading: false });
    const onFinish = async (values: any) => {
        updateState((draft: any) => {
            draft.loading = true;
        });
        try {
            const result = await login(values.email, values.password, values.remember);
            if (!result) {
                notification.warn({ message: <LocaleHelp tkey={"login.password.invalid"} /> });
                return;
            }
            setTimeout(() => (window.location.href = `/welcome`), 0);
        } finally {
            updateState((draft: any) => {
                draft.loading = false;
            });
        }
    };

    // noinspection HtmlUnknownTarget
    // @ts-ignore
    return (
        <FormStyle>
            <Style>
                <h1>
                    <LocaleTitle tkey={"login"} />
                </h1>
                <Form
                    form={form}
                    className="login-form"
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    style={{ maxWidth: "450px", margin: "auto" }}
                >
                    <Form.Item>
                        <FormHeader>
                            <LocaleHelp tkey={"login"} />
                        </FormHeader>
                    </Form.Item>
                    <Form.Item
                        name="email"
                        initialValue={props.email}
                        rules={[{ required: false, message: t("login.email.error") }]}
                    >
                        <Input
                            disabled={!!props.email}
                            size="large"
                            prefix={
                                <>
                                    <UserOutlined {...({} as any)} />
                                </>
                            }
                            placeholder={LocalePlaceHolder({ tkey: "userName" })}
                        />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                min: 10,
                                message: "Please enter your password (min 10 characters)",
                            },
                        ]}
                    >
                        <Input
                            placeholder={LocalePlaceHolder({ tkey: "password" })}
                            size="large"
                            type={"password"}
                            suffix={<LockOutlined {...({} as any)} />}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button
                            style={{ width: "100%" }}
                            size="large"
                            type={"primary"}
                            loading={state.loading}
                            onClick={() => form.submit()}
                        >
                            <LocaleButton tkey={"login"} />
                        </Button>
                    </Form.Item>
                    <Form.Item>
                        <Form.Item name="remember" valuePropName="checked" noStyle>
                            <Checkbox>
                                <LocaleLabel tkey={"remember.me"} />
                            </Checkbox>
                        </Form.Item>

                        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                        <a className="login-form-forgot" href="/forgot-password-email">
                            <LocaleButton tkey={"forgotPasswordInLogin"} />
                        </a>
                    </Form.Item>
                </Form>
            </Style>
        </FormStyle>
    );
}
