import {
    MailOutlined,
    PhoneOutlined,
    SafetyCertificateOutlined,
    UserOutlined,
} from "@ant-design/icons";
import { Button, Card, Form, Input, Select } from "antd";
// @ts-ignore
import { navigate } from "hookrouter";
// @ts-ignore
import React, { useContext } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import styled from "styled-components";
import { useImmer } from "use-immer";
import { FormHeader } from "../../components/form-header";
import { FormStyle, layout, tailLayout } from "../../components/form-layout";
import { useUser } from "../../hooks/user";
import { UserContextType } from "../../interfaces";
import { LocaleHelp, LocaleLabel, LocaleTitle } from "../../locale";
import { Translate } from "../../translation-hook";
import { UserContext } from "../../user-context";

export type Field = "gender" | "firstName" | "lastName" | "phone" | "companyName";
const Style = styled(Card).attrs({ bordered: false })``;

export function RegisterForm(
    props: {
        code?: string;
        email?: string;
        hiddenFields?: Field[];
    } = { hiddenFields: [] }
) {
    const { t } = Translate();
    const [state, updateState] = useImmer({ captcha: false, loading: false });
    const { register } = useUser(useContext<UserContextType>(UserContext));
    const onFinish = async (values: any) => {
        try {
            updateState((draft: any) => {
                draft.loading = true;
            });
            await register(values);
            navigate(`/login/${values.email}`);
        } catch (e) {
            throw e;
        } finally {
            updateState((draft: any) => {
                draft.loading = false;
            });
        }
    };

    function isVisible(fieldName: Field) {
        return !props.hiddenFields?.some((d) => d === fieldName);
    }

    return (
        <FormStyle>
            <Style>
                <h1>
                    <LocaleTitle tkey={"signup"} />
                </h1>
                <Form onFinish={onFinish} {...layout}>
                    <Form.Item>
                        <FormHeader>
                            <LocaleHelp tkey={"signup"} />
                        </FormHeader>
                    </Form.Item>
                    {isVisible("gender") && (
                        <Form.Item
                            name={"gender"}
                            label={<LocaleLabel tkey={"gender"} />}
                            required={true}
                        >
                            <Select
                                options={[
                                    { value: "m", label: t("gender.male") },
                                    { value: "f", label: t("gender.female") },
                                    { value: "o", label: t("gender.other") },
                                ]}
                            />
                        </Form.Item>
                    )}

                    <div style={{ display: "flex", gap: "15px" }}>
                        {isVisible("firstName") && (
                            <Form.Item
                                style={{ flex: "1 1" }}
                                name={"firstName"}
                                label={<LocaleLabel tkey={"firstName"} />}
                                required={true}
                            >
                                <Input prefix={<UserOutlined {...({} as any)} />} />
                            </Form.Item>
                        )}
                        {isVisible("lastName") && (
                            <Form.Item
                                style={{ flex: "1 1" }}
                                name={"lastName"}
                                label={<LocaleLabel tkey={"lastName"} />}
                                required={true}
                            >
                                <Input prefix={<UserOutlined {...({} as any)} />} />
                            </Form.Item>
                        )}
                    </div>
                    {isVisible("companyName") && (
                        <Form.Item
                            name={"companyName"}
                            label={<LocaleLabel tkey={"companyName"} />}
                            required={true}
                        >
                            <Input />
                        </Form.Item>
                    )}
                    {isVisible("phone") && (
                        <Form.Item
                            name={"phone"}
                            label={<LocaleLabel tkey={"phone"} />}
                            required={false}
                            rules={[{ type: "string" }]}
                        >
                            <Input prefix={<PhoneOutlined {...({} as any)} />} />
                        </Form.Item>
                    )}
                    <Form.Item
                        hidden={true}
                        name={"code"}
                        label={<LocaleLabel tkey={"code"} />}
                        initialValue={props.code}
                        required={true}
                    >
                        <Input
                            prefix={<SafetyCertificateOutlined {...({} as any)} />}
                            type={"number"}
                        />
                    </Form.Item>
                    <Form.Item
                        name={"email"}
                        label={<LocaleLabel tkey={"email"} />}
                        initialValue={props.email}
                        required={true}
                        rules={[{ type: "string" }]}
                    >
                        <Input disabled={true} prefix={<MailOutlined {...({} as any)} />} />
                    </Form.Item>
                    <Form.Item
                        name={"password"}
                        label={<LocaleLabel tkey={"password"} />}
                        required={true}
                        rules={[
                            {
                                min: 10,
                                required: true,
                                message: "Please enter your password (min 10 characters)",
                            },
                        ]}
                    >
                        <Input
                            type={"password"}
                            prefix={<SafetyCertificateOutlined {...({} as any)} />}
                        />
                    </Form.Item>

                    <Form.Item labelCol={{ span: 0 }} wrapperCol={{ span: 24 }} required={true}>
                        <div style={{ width: "fit-content", marginRight: 0, marginLeft: "auto" }}>
                            <ReCAPTCHA
                                size={"normal"}
                                sitekey="6LdxSx0aAAAAAITN1byVT-_x8ihn8spFZT9tbgyF"
                                onChange={() =>
                                    updateState((draft: any) => {
                                        draft.captcha = true;
                                    })
                                }
                            />
                        </div>
                    </Form.Item>
                    <Form.Item {...tailLayout}>
                        <Button
                            style={{ width: "100%" }}
                            size="large"
                            type={"primary"}
                            loading={state.loading}
                            disabled={!state.captcha}
                            htmlType={"submit"}
                        >
                            <LocaleLabel tkey={"register"} />
                        </Button>
                    </Form.Item>
                    <Form.Item>
                        <div className={"cgu"}>
                            <LocaleHelp tkey={"signup.footer"} />
                        </div>
                    </Form.Item>
                </Form>
            </Style>
        </FormStyle>
    );
}
