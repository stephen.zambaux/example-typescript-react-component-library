import { UserOutlined } from "@ant-design/icons";
import { Card, Form } from "antd";
import { useForm } from "antd/lib/form/Form";
import Search from "antd/lib/input/Search";
// @ts-ignore
import { navigate } from "hookrouter";
// @ts-ignore
import React, { useContext } from "react";
import styled from "styled-components";
import { useImmer } from "use-immer";
import { FormHeader } from "../../components/form-header";
import { FormStyle } from "../../components/form-layout";
import { useUser } from "../../hooks/user";
import { UserContextType } from "../../interfaces";
import { LocaleButton, LocaleHelp, LocaleTitle } from "../../locale";
import { UserContext } from "../../user-context";

const Style = styled(Card).attrs({ bordered: false })`
    max-width: 500px;
    margin: auto;
    flex: none;
`;

export function SignInUpForm() {
    const { registerEmail, preLogin } = useUser(useContext<UserContextType>(UserContext));
    const [form] = useForm();
    // @ts-ignore
    const [state, updateState] = useImmer({ loading: false });
    const onFinish = async (values: any) => {
        updateState((draft: any) => {
            draft.loading = true;
        });
        try {
            const userExists = await preLogin(values.email);
            if (!userExists) {
                await registerEmail(values.email);
                navigate(`/register-code/${values.email}`);
            } else {
                navigate(`/login/${values.email}`);
            }
        } finally {
            updateState((draft: any) => {
                draft.loading = false;
            });
        }
    };

    // noinspection HtmlUnknownTarget
    // @ts-ignore
    return (
        <FormStyle>
            <Style>
                <h1>
                    <LocaleTitle tkey={"connect"} />
                </h1>
                <Form form={form} name="control-hooks" onFinish={onFinish}>
                    <Form.Item>
                        <FormHeader><LocaleHelp tkey={"connect"} /></FormHeader>
                    </Form.Item>
                    <Form.Item name={"email"} rules={[{ type: "email", required: true }]}>
                        <Search
                            placeholder="paul@gmail.com"
                            enterButton={<LocaleButton tkey={"connect"} />}
                            size="large"
                            loading={state.loading}
                            type={"email"}
                            suffix={<UserOutlined {...({} as any)} />}
                            onSearch={() => form.submit()}
                        />
                    </Form.Item>
                </Form>
            </Style>
        </FormStyle>
    );
}
