import axios from "axios";
import { Namespace, useTranslation, UseTranslationOptions } from "react-i18next";

import i18next from 'i18next';
import HttpApi from 'i18next-http-backend';
import {initReactI18next} from "react-i18next";

const alreadySaved: any[]= []

i18next
    .use(initReactI18next)
    .use(HttpApi)
    .init({
        lng: "fr",
        fallbackLng: 'fr',
        debug: true,
        keySeparator: false,
        interpolation: {
            escapeValue: false
        },
        saveMissing: true,
        updateMissing: true,
        // @ts-ignore
        missingKeyHandler: (lngs: string[], ns: string, key: string) => {
            if (key) {
                if (alreadySaved.indexOf(key) === -1) {
                    alreadySaved.push(key)
                    axios.post('/api/locales', {id: key})
                }
            }
        },
        backend: {
            loadPath: '/api/public/locales',
            allowMultiLoading: false, // set loadPath: '/locales/resources.json?lng={{lng}}&ns={{ns}}' to adapt to multiLoading
        }
    }, () => undefined);

export const Translate = (
    ns?: Namespace,
    options?: UseTranslationOptions
): { t: (key: string) => string } => {
    const { t } = useTranslation(ns, options);
    return { t: (key) => t(key) || "" };
};
